'''
Author      : now more
Connect     : lin.honghui@qq.com
LastEditors: Please set LastEditors
Description :
LastEditTime: 2020-11-27 03:42:46
'''
import copy
import glob
import os
import threading
import cv2 as cv
import numpy as np
from skimage.morphology import remove_small_holes, remove_small_objects
from argparse import ArgumentParser
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm

def mask_replace_num(mask,originNum,newNum):
    masktmp = copy.deepcopy(mask)
    if originNum in mask:
        masktmp[np.where(mask == originNum)] = newNum
    return masktmp

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-gt_dir", type=str,
                        default=r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\suichang_round1_train_210120",  # /home/admin/tianchi/tianchi-logic-object/data/suichang_round1_train_210120/
                        help="train的人工标注好的数据集的png")
    parser.add_argument("-update_gt_dir", type=str,
                        default=r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\update_suichang_round1_train_210120", # /home/admin/tianchi/tianchi-logic-object/data/update_suichang_round1_train_210120/
                        help="更新后的的标注数据存放路径")
    arg = parser.parse_args()
    gt_dir = arg.gt_dir
    update_gt_dir = arg.update_gt_dir

    if not os.path.exists(update_gt_dir):
        os.makedirs(update_gt_dir)


    labelsjson = {
        1: "耕地",
        2: "林地",
        3: "草地", # grass
        4: "道路", # road
        5: "城镇建设用地",
        6: "农村建设用地",
        7: "工业用地",
        8: "构筑物",# construction
        9: "水域",
        10: "裸地" # bareland
    }

    for gt_dir_mask_path in tqdm(glob.glob(os.path.join(gt_dir,'*.png'))):
        mask_name = os.path.basename(gt_dir_mask_path)
        gt_dir_mask = np.asarray(Image.open(gt_dir_mask_path))
        update_gt_dir_mask_path = os.path.join(update_gt_dir, mask_name)

        # gt_dir_mask = mask_replace_num(gt_dir_mask,1,1)
        # gt_dir_mask = mask_replace_num(gt_dir_mask,2,2)
        gt_dir_mask = mask_replace_num(gt_dir_mask,3,0)
        gt_dir_mask = mask_replace_num(gt_dir_mask,4,0)
        gt_dir_mask = mask_replace_num(gt_dir_mask,5,3)
        gt_dir_mask = mask_replace_num(gt_dir_mask,6,4)
        gt_dir_mask = mask_replace_num(gt_dir_mask,7,5)
        gt_dir_mask = mask_replace_num(gt_dir_mask,8,0)
        gt_dir_mask = mask_replace_num(gt_dir_mask,9,6)
        gt_dir_mask = mask_replace_num(gt_dir_mask,10,0)
        cv.imwrite(update_gt_dir_mask_path, gt_dir_mask)

        # mask_post = label_resize_vis(label, source_image)
        # cv.imwrite(os.path.join(r"C:\Users\zengxh\Desktop\viresult",mask_name), mask_post)
