### 预测softpool

```shell
conda deactivate
conda activate yolact_py38_yolov5
cd /home/admin/tianchi/tianchi-logic-object
rm -rf mmsegmentation
cp -r mmsegmentationsoftpool3693 mmsegmentation
cd mmsegmentation

python -m torch.distributed.launch --nproc_per_node 2 --master_addr 127.0.0.1 --master_port 29501 tools/test.py configs/TianchiSeg/baseline.py /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/latest.pth --launcher pytorch 

cd /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/results/

zip -r results.zip ./*

mv results.zip ../

cd ..
```


### 预测0.3927

```shell
conda deactivate
conda activate yolact_py38_yolov5
cd /home/admin/tianchi/tianchi-logic-object
rm -rf mmsegmentation
cp -r mmsegmentation3927/ mmsegmentation
cd mmsegmentation

python -m torch.distributed.launch --nproc_per_node 2 --master_addr 127.0.0.1 --master_port 29501 tools/test.py configs/TianchiSeg/baseline.py /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_120000.pth --launcher pytorch 

cd /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/results/

zip -r results.zip ./*

mv results.zip ../

cd ..
```



### 集成

```shell
C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\integrate\integrate-finally-model.py

C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\cvkmeans3.py

# 50次
C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\integrate-cvkmeans-all.py
```

## 沈的结果
### 预测0.3879

```shell
conda deactivate
conda activate yolact_py38_yolov5
cd /home/admin/tianchi/tianchi-logic-object
rm -rf mmsegmentation
cp -r mmsegmentation3879model3/ mmsegmentation
cd mmsegmentation

python -m torch.distributed.launch --nproc_per_node 2 --master_addr 127.0.0.1 --master_port 29501 tools/test.py configs/TianchiSeg/baseline.py /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_120000.pth --launcher pytorch 

cd /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/results/

zip -r results.zip ./*

mv results.zip ../

cd ..
```

### 预测0.3807

```shell
conda deactivate
conda activate yolact_py38_yolov5
cd /home/admin/tianchi/tianchi-logic-object
rm -rf mmsegmentation
cp -r mmsegmentation3807/ mmsegmentation
cd mmsegmentation

python -m torch.distributed.launch --nproc_per_node 2 --master_addr 127.0.0.1 --master_port 29501 tools/test.py configs/TianchiSeg/baseline.py /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_120000.pth --launcher pytorch 

cd /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/results/

zip -r results.zip ./*

mv results.zip ../

cd /home/admin/tianchi/tianchi-logic-object
```

### 预测4分类

```shell
conda deactivate
conda activate yolact_py38_yolov5
cd /home/admin/tianchi/tianchi-logic-object
rm -rf mmsegmentation
cp -r mmsegmentationmodel1/ mmsegmentation
cd mmsegmentation

python -m torch.distributed.launch --nproc_per_node 2 --master_addr 127.0.0.1 --master_port 29501 tools/test.py configs/TianchiSeg/baseline.py /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_120000.pth --launcher pytorch 

cd /home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/results/

zip -r results.zip ./*

mv results.zip ../

cd /home/admin/tianchi/tianchi-logic-object
```

### 集成

```shell
C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\integrate\integrate11.py

C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\cvkmeans3.py
```

