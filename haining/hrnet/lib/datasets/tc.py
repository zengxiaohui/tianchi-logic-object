# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Written by Ke Sun (sunk@mail.ustc.edu.cn)
# ------------------------------------------------------------------------------

import os

import cv2
import numpy as np

import torch
from torch.nn import functional as F
from torch.utils import data

from .base_dataset import BaseDataset
import albumentations as A
from albumentations.pytorch import ToTensorV2

image_size = 256
max_size = int(image_size * 2.0) # 放大到最大的比例
middle_size = int(image_size * 1.25)#初始1.5 1.75 中间尺寸比例

train_transform = A.Compose(
    [
        A.OneOf(
            [
                A.Compose([
                    A.LongestMaxSize(max_size, p=1),
                    A.RandomCrop(image_size, image_size, p=1)
                ]),
                A.Compose([
                    A.Resize(image_size, image_size, p=1)
                ]),
                A.Compose([
                    A.LongestMaxSize(middle_size, p=1),
                    A.RandomCrop(image_size, image_size, p=1)
                ]),
            ],
            p=0.5,
        ),
        A.VerticalFlip(p=0.5),
        A.HorizontalFlip(p=0.5),
        A.RandomRotate90(p=0.5),
        # color transforms
        A.OneOf(
            [
                A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.2, p=1),
                A.RandomGamma(gamma_limit=(80, 120), p=1),
                A.ChannelShuffle(p=0.2),
            ],
            p=0.8,
        ),
        # distortion
        A.OneOf(
            [
                A.ElasticTransform(p=1),
                A.OpticalDistortion(p=1),
                A.GridDistortion(p=1),
                A.IAAPerspective(p=1),
            ],
            p=0.2,
        ),
        # noise transforms
        A.OneOf(
            [
                A.GaussNoise(p=1),
                A.IAASharpen(p=1),
                A.MultiplicativeNoise(p=1),
                A.GaussianBlur(p=1),
            ],
            p=0.2,
        ),
        A.Normalize(mean=(0.485, 0.456, 0.406, 0.5), std=(0.229, 0.224, 0.225, 0.25)),
        ToTensorV2(),
    ]
)

val_transform = A.Compose(
    [
        A.Normalize(mean=(0.485, 0.456, 0.406, 0.5), std=(0.229, 0.224, 0.225, 0.25)),
        ToTensorV2(),
    ]
)


class TC(data.Dataset):
    def __init__(self, root, list_path, num_samples=None, num_classes=10, **kwargs):
        if "train" in list_path:
            self.transform = train_transform
        else:
            self.transform = val_transform
        self.root = root
        self.num_classes = num_classes
        self.list_path = list_path  # image_path label_path
        self.class_weights = None

        self.img_list = [line.strip().split() for line in open(root + list_path)]

        self.files = self.read_files()
        if num_samples:
            self.files = self.files[:num_samples]

    def read_files(self):
        files = []
        for item in self.img_list:
            image_path, label_path = item[:2]
            name = os.path.splitext(os.path.basename(label_path))[0]
            sample = {
                "img": image_path,
                "label": label_path,
                "name": name,
            }
            files.append(sample)
        return files

    def __len__(self):
        return len(self.files)

    def __getitem__(self, index):
        item = self.files[index]
        name = item["name"]
        image = cv2.imread(item["img"], -1)
        label = cv2.imread(
            item["label"],
            cv2.IMREAD_GRAYSCALE,
        )
        label -= 1
        transformed = self.transform(image=image, mask=label)
        image = transformed["image"]
        mask = transformed["mask"]
        return image, mask.long(), 0, 0

    def inference(self, model, image):
        size = image.size()
        pred = model(image)
        pred = F.interpolate(
            input=pred, size=(size[-2], size[-1]), mode="bilinear", align_corners=True
        )
        return pred.exp()
