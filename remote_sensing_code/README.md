### 比赛
1. [2021全国数字生态创新大赛-智能算法赛](https://tianchi.aliyun.com/competition/entrance/531860/introduction)

### 比赛参考
1. [Weighted Boxes Fusion](https://blog.csdn.net/qq_41131535/article/details/106898081)

### 本地docker测试代码
```shell script
# 构建build
cd /home/deploy/tianchi-logic-object/remote_sensing_code/
sed -i 's/\r//' run.sh
sudo docker build -t="remote_sensing:v1" -f Dockerfile .
# 运行后进入
sudo docker run -t -i -p 8080:8080 -p 1022:22 -p 6606:6006 --ipc=host -v /home/deploy/tianchi/submit/tcdata/:/tcdata -v /home/deploy/tianchi/submit/user_data/:/user_data -v /home/deploy/tianchi-logic-object/remote_sensing_code/external_datas/:/external_datas --gpus all remote_sensing:v1 /bin/bash
docker run -t -i -p 8080:8080 -p 1022:22 -p 6606:6006 --ipc=host -v C:/Users/zengxh/Documents/workspace/PyCharm-workspace/tianchi/tianchi-logic-object/data/:/tcdata -v C:/Users/zengxh/Documents/workspace/PyCharm-workspace/tianchi/tianchi-logic-object/data/:/user_data --gpus all remote_sensing:v2 /bin/bash 
service ssh restart
#后台运行此容器并且返回容器id 这样就会运行dockerfile中的cmd
sudo docker run -d -p 8080:8080 -p 1022:22 -p 6606:6006 --ipc=host -v /home/deploy/tianchi/submit/tcdata/:/tcdata -v /home/deploy/tianchi/submit/user_data/:/user_data -v /home/deploy/tianchi-logic-object/remote_sensing_code/external_datas/:/external_datas --gpus all remote_sensing:v1
# 查看端口监听情况
sudo docker exec -t 073034e15571 netstat -an | grep LISTEN | grep 8080
sudo docker port c8cc1bde1d70 8080
# 进入正在运行的容器
sudo docker start 67bb317afef3
sudo docker exec -it c7337f64da2d bash 
# 测试是否能用
/net1/bin/python home.py
# 文件copy
sudo docker cp /home/deploy/tianchi-logic-object/remote_sensing_code/infer.py e7a3b6ee9bc8:infer.py
sudo docker cp 700758f3576b:/user_data/tmp_data/results3967/000013.png  /home/deploy/tianchi-logic-object/remote_sensing_code/
# 文件夹copy
sudo docker cp /home/deploy/tianchi-logic-object/remote_sensing_code/ours_code/net1 09ca83de2ee2:/ours_code/
```

### 登录docker阿里库
```shell script
sudo docker login --username=2362651588@qq.com registry.cn-shanghai.aliyuncs.com
# 拉取
# sudo docker pull registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:[镜像版本号 tag]
# build
cd /home/deploy/tianchi-logic-object/remote_sensing_code/
sed -i 's/\r//' run.sh
sudo docker build -t="remote_sensing:v1" -f Dockerfile .
# tag
sudo docker tag cba90ffad791 registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:vhr1
# 推送
sudo docker push registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:vhr1
```

#### 训练
```shell script
# 查看当前目录中文件个数
ls -l | grep "^-" | wc -l
```

#### 提交异常分析
> 异常1 {'message': 'Back-off pulling image ' '"registry.cn-shanghai.aliyuncs.com/"', 'reason': 'ImagePullBackOff'}
> 解决1 这个是拉取异常 registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:[镜像版本号 tag] 是否设置正确，本地试试能否拉取成功
> 异常2 {'message': None, 'reason': 'PodInitializing'}
> 解决2 PodInitializing 初始中 等待，刷新页面会出现running
> 异常3 RuntimeError: Found no NVIDIA driver on your system. Please check that you have an NVIDIA GPU and installed a driver from http://www.nvidia.com/Download/index.aspx
> 解决3 registry.cn-shanghai.aliyuncs.com/tcc-public/pytorch:1.4-cuda10.1-py3 

### docker 
```shell script
# 构建build
sudo docker build -t="remote_sensing:v1" -f Dockerfile .
# 删除镜像
sudo docker rmi -f remote_sensing:v1
# 查看镜像
sudo docker images
# 通过镜像运行容器
sudo docker run -d --ipc=host remote_sensing:v1  #后台运行此容器并且返回容器id 这样就会运行dockerfile中的cmd
sudo docker run -t -i --ipc=host remote_sensing:v1 /bin/bash
# 查看所有容器
sudo docker ps -a
# 查看容器启动日志
sudo docker logs 543a2d3374043ad191
# 停止正在运行的容器
sudo docker stop c477499ce298 #停止运行某个容器f4为容器id
# 进入容器
sudo docker exec -it 734d399cf0b6a43accf1e6d3f0e4b675ad927af21ad3a1336cc921dc86f8b171 bash #在终端进入容器进行操作f4为容器id
# 删除容器
sudo docker rm 容器id
# 删除镜像
sudo docker rmi -f remote_sensing:v1
# 删除所有镜像
sudo docker image prune -f -a 
# 删除所有容器
sudo docker container prune -f
# 拷贝本地文件到容器
sudo docker cp 你的文件路径 容器长ID:docker容器路径
# 从容器里面拷文件到宿主机
sudo docker cp 容器名:要拷贝的文件在容器里面的路径  要拷贝到宿主机的相应路径 
# 修改容器的root密码
$PASS='<a-good-password>'
echo -e "$PASS\n$PASS" | sudo docker exec -i <container-id-or-name> passwd
```

### docker 阿里大赛使用
1. [入门Docker练习场常见问题-天池大赛-阿里云天池](https://tianchi.aliyun.com/competition/entrance/231759/tab/174?spm=5176.12281976.0.0.567c491cTtkJ9A)
2. [在IntelliJ IDEA中安装和配置Cloud Toolkit](https://help.aliyun.com/document_detail/98762.html?spm=5176.12586973.0.0.6fd92232aEoRLl)
3. [获取AccessKey](https://help.aliyun.com/document_detail/145787.htm?spm=a2c4g.11186623.2.13.18e766fardorp6#task-2349932)
4. [大赛基础镜像](https://tianchi.aliyun.com/forum/postDetail?postId=67720)
5. [pycharm 配置远程docker](https://www.cnblogs.com/kai-/p/12811036.html)
6. [我的容器镜像服务](https://cr.console.aliyun.com/repository/cn-shanghai/zengxiaohui/remote_sensing/details)
7. [视频学习](https://tianchi.aliyun.com/course/351?spm=5176.12586973.0.0.4e772ea8yOOvlr)

### 比赛得分
> Fe= 0.4*(1-(min(max(x,x_{min}),x_{max})-x_{min})/(x_{max}-x_{min})) 
> final_score= 0.7mIoU+ 0.3Fe
> 要想Fe高需要 单张图片的预测 特别快

#### 上分参考链接
1. [UNet-Pruning](https://github.com/XuYongi/UNet-Pruning)
2. [模型裁剪](https://github.com/666DZY666/micronet)
3. [数据增强](https://www.kaggle.com/cdeotte/cutmix-and-mixup-on-gpu-tpu)

#### tensorrt
1. [cuda](https://developer.nvidia.com/cuda-10.2-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804&target_type=runfilelocal)
2. [tensorrt](https://developer.nvidia.com/nvidia-tensorrt-7x-download)
3. [cudnn](https://developer.nvidia.com/rdp/cudnn-archive)

#### 修改docker默认存储位置
```shell script
# 查看存储位置
docker info
#停止docker服务
sudo systemctl stop docker
#修改docker服务启动文件
/usr/lib/systemd/system/docker.service
/etc/docker/daemon.json

sudo systemctl daemon-reload
sudo systemctl start docker
sudo systemctl status docker

cp -rf /var/lib/docker/* /home/deploy/dockerDIR/
```

##### 容器提交为镜像
```shell script
sudo docker save -o aitechnology_pvdefect.tar aitechnology/pvdefect:v1 # 将镜像导出为文件
docker load --input aitechnology_pvdefect.tar #导入镜像

sudo docker commit 98ebdd62aca6 remote_sensing:vhrrt1-3 # 如果有镜像可以不用这个 这个是将容器提交为镜像
# 重新构建带环境变量的可以提交的
cd /home/deploy/tianchi-logic-object/remote_sensing_code/
sudo docker build -t="remote_sensing:vhrrt2-3.2" -f Dockerfile2 .
# tag
sudo docker tag 36f38771a7ef registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:vhrrt3-3.2
#测试
sudo docker run -d -p 8080:8080 -p 1022:22 -p 6606:6006 --ipc=host -v /home/deploy/tianchi/submit/tcdata/:/tcdata -v /home/deploy/tianchi/submit/user_data/tmp_data/:/user_data/tmp_data -v /home/deploy/tianchi-logic-object/remote_sensing_code/external_datas/:/external_datas --gpus all registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:vhrrt3-3.2
# 推送
sudo docker push registry.cn-shanghai.aliyuncs.com/zengxiaohui/remote_sensing:vhrrt3-3.2

```

#### docker容器访问不了外网解决办法
```shell script
ip addr 
# 找到mtu 后面是多少1500
# 那么app-network是自己定义的
docker network create --opt com.docker.network.driver.mtu=1500 app-network
```