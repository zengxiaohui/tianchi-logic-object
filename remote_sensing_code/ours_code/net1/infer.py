# -*- coding: utf-8 -*-
import numpy as np
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.autograd import Variable
import cv2
import time
from io import BytesIO
import base64
from PIL import Image

from ours_code.net1.models.model import seg_qyl
from ours_code.net1.utils import colorEncode

Image.MAX_IMAGE_PIXELS = 1000000000000000
from tqdm import tqdm, trange
import glob
import os
from scipy.io import loadmat
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import segmentation_models_pytorch as smp
import albumentations as A
from albumentations.pytorch import ToTensorV2
import torch.nn as nn
from torch.cuda.amp import autocast
USE_CUDA = torch.cuda.is_available()
device = torch.device("cuda:0" if USE_CUDA else "cpu")

def visualize_result(img_dir, pred):
    #
    img=cv2.imread(img_dir)
    colors = loadmat('demo/color150.mat')['colors']
    names = {
            1: "耕地",
            2: "林地",
            3: "草地",
            4: "道路",
            5: "城镇建设用地",
            6: "农村建设用地",
            7: "工业用地",
            8: "构筑物",
            9: "水域",
            10: "裸地"
        }
    # print predictions in descending order
    pred = np.int32(pred)
    pixs = pred.size
    uniques, counts = np.unique(pred, return_counts=True)
    #
    for idx in np.argsort(counts)[::-1]:
        name = names[uniques[idx] + 1]
        ratio = counts[idx] / pixs * 100
        if ratio > 0.1:
            print("  {}: {:.2f}%".format(name, ratio))

    # colorize prediction
    pred_color = colorEncode(pred, colors).astype(np.uint8)

    # aggregate images and save
    #print(pred_color.shape)
    #pred_color=cv2.resize(pred_color,(256,256))
    im_vis = np.concatenate((img, pred_color), axis=1)

    #
    #img_name=image_demo_dir.split('/')[-1]
    save_dir,name=os.path.split(img_dir)
    Image.fromarray(im_vis).save('demo/256x256_deeplab_44.png')
def get_infer_transform():
    transform = A.Compose([
        A.Resize(256, 256),
        # A.FDA([target], p=1, read_fn=lambda x: x),
        # A.Normalize(mean=(0.485*0.884, 0.456*0.7977, 0.406*0.9495), std=(0.229*0.99, 0.224*0.95, 0.225*1.0685)),
        A.Normalize(mean=(0.485, 0.456, 0.406),
                    std=(0.229, 0.224, 0.225)),
        ToTensorV2(),
    ])
    return transform

def inference(img_dir):
    # target = cv2.imread(target_img_dir, cv2.IMREAD_COLOR)
    # target = cv2.cvtColor(target, cv2.COLOR_BGR2RGB)
    transform=get_infer_transform()
    image = cv2.imread(img_dir, cv2.IMREAD_COLOR)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    img = transform(image=image)['image']
    img = img.unsqueeze(0)
    #print(img.shape)
    with torch.no_grad():
        img = img.cuda()
        output = model(img)
    #
    pred = output.squeeze().cpu().data.numpy()
    pred = np.argmax(pred,axis=0)
    return pred+1


def moving_average(net1, net2, net3):
    for param1, param2, param3 in zip(net1.parameters(), net2.parameters(), net3.parameters()):
        param1.data *= 0.6
        param1.data += param2.data * 0.3 + param3.data * 0.1

if __name__=="__main__":
    model_name = 'resnet50'#'efficientnet-b6'
    n_class=10
    model=seg_qyl(model_name,n_class)
    # model= torch.nn.DataParallel(model)
    checkpoints=torch.load('/user_data/model_data/checkpoint-best.pth')
    model.load_state_dict(checkpoints['state_dict'])
    model.to(device)

    #moving_average(model,SWAmodel1,SWAmodel2)

    model.eval()
    # torch_sobel = Sobel().cuda()
    # torch_sobel = torch.nn.DataParallel(torch_sobel)
    use_demo=False
    assert_list=[1,2,3,4,5,6,7,8,9,10]
    if use_demo:
        img_dir='demo/000097.jpg'
        pred=inference(img_dir)
        infer_start_time = time.time()
        visualize_result(img_dir, pred)
        #
    else:
        out_dir='/user_data/tmp_data/results3967/'
        if not os.path.exists(out_dir):os.makedirs(out_dir)
        test_paths=glob.glob('/tcdata/suichang_round1_test_partB_210120/*')
        # test_paths = glob.glob('/dat01/liuweixing/tianchi/img_dir/train_val/*')


        for i in trange(len(test_paths)):
            jpg_file = test_paths[i]
            result=inference(jpg_file)
            result = np.array(result).astype(np.uint8)

            # tif_path = os.path.join(tiff_data_root,jpg_file.split('/')[-1][:-4]+'.tif')
            # print(tif_path)
            # tifimg = cv2.imread(tif_path, -1)
            # nir = tifimg[:, :, 3]

            # result[np.array(result == 10)] = 5
            # result[np.array(result == 3)] = 2

            img=Image.fromarray(result)

            img=img.convert('L')
            img = img.resize((256, 256),resample=Image.NEAREST)
            #print(out_path)
            out_path=os.path.join(out_dir,jpg_file.split('/')[-1][:-4]+'.png')
            # out_path = os.path.join(out_dir, per_path.split('\\')[-1][:-4] + '.png')
            img.save(out_path)
