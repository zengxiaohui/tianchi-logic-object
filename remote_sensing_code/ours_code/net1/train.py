import sys
sys.path.insert(0, '/')
from pathlib import Path
import argparse
import yaml

import os
import torch
import numpy as np
import torch.nn as nn
from PIL import Image
import os
import time
import copy
import torch
import random
import logging
import numpy as np
import torch.nn as nn
import torch.optim as optim

from glob import glob
from PIL import Image
from tqdm import tqdm

from torch.autograd import Variable
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import Dataset, DataLoader
from torchvision.transforms import functional
from torch.utils.data import WeightedRandomSampler

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from pytorch_toolbelt import losses as L

from ours_code.net1.utils.general import increment_dir
from ours_code.net1.models.model import seg_qyl
from ours_code.net1.utils import IOUMetric
from ours_code.net1.utils.torch_utils import select_device, init_seeds
from ours_code.net1.dataset import RSCDataset
from ours_code.net1.dataset import train_transform, val_transform
from ours_code.net1.dataset.transform import mixup_criterion, mixup_data
from ours_code.net1.utils.utils import AverageMeter, second2time, inial_logger
from albumentations.augmentations import functional as F
from torch.cuda.amp import autocast, GradScaler#need pytorch>1.6
# from segmentation_models_pytorch.losses import DiceLoss,FocalLoss,SoftCrossEntropyLoss
from segmentation_models_pytorch import losses as losses

from catalyst.contrib.nn import IoULoss
from ours_code.net1.utils.jointloss import JointLoss2,JointLoss3
from tensorboardX import SummaryWriter

import logging
import segmentation_models_pytorch as smp
Image.MAX_IMAGE_PIXELS = 1000000000000000

from collections import OrderedDict

logger = logging.getLogger(__name__)


def smooth(v, w=0.85):
    last = v[0]
    smoothed = []
    for point in v:
        smoothed_val = last * w + (1 - w) * point
        smoothed.append(smoothed_val)
        last = smoothed_val
    return smoothed

def train(param, opt,model, train_data, valid_data, plot=False, device='cuda'):
    # 初始化参数
    model_name = param['model_name']
    lr = param['lr']
    gamma = param['gamma']
    step_size = param['step_size']
    momentum = param['momentum']
    weight_decay = param['weight_decay']

    disp_inter = param['disp_inter']
    save_inter = param['save_inter']
    min_inter = param['min_inter']
    iter_inter = param['iter_inter']

    log_dir = param['log_dir']
    save_ckpt_dir = param['save_ckpt_dir']
    load_ckpt_dir = param['load_ckpt_dir']

    init_seeds(2)
    summarywriter = SummaryWriter(log_dir)
    print("/net1/bin/tensorboard --logdir={} --port=6006 --host=0.0.0.0".format(os.path.abspath(log_dir)))

    #
    scaler = GradScaler()

    # 网络参数
    train_data_size = train_data.__len__()
    valid_data_size = valid_data.__len__()
    c, y, x = train_data.__getitem__(0)['image'].shape

    # img_weight = np.load('/dat01/liuweixing/tianchi/unet_eff/img_weight.npy')
    # sampler_weights = torch.from_numpy(img_weight)
    # 根据类别丰富度对样本采样
    # logger.info('sampler_weights:{}'.format(sampler_weights[0]))
    # sampler = WeightedRandomSampler(weights=sampler_weights, num_samples=len(train_data), replacement=True)

    train_loader = DataLoader(dataset=train_data, batch_size=opt.batch_size, shuffle=True, num_workers=32, pin_memory=True)
    # train_loader = DataLoader(dataset=train_data, batch_size=batch_size, sampler=sampler, shuffle=False, num_workers=1)
    valid_loader = DataLoader(dataset=valid_data, batch_size=opt.batch_size, shuffle=False, num_workers=32)

    optimizer = optim.AdamW(model.parameters(), lr=3e-4, weight_decay=weight_decay)
    # optimizer = optim.SGD(model.parameters(), lr=3e-4, momentum=momentum, weight_decay=weight_decay)

    # scheduler = StepLR(optimizer, step_size=step_size, gamma=gamma)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0=6, T_mult=2, eta_min=1e-5,
                                                                     last_epoch=-1)
    # scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', factor=0.25, patience=2)

    IoULoss_fn = IoULoss()
    lovasz_fn = losses.LovaszLoss(mode='multiclass')
    DiceLoss_fn = losses.DiceLoss(mode='multiclass')
    SoftCrossEntropy_fn = losses.SoftCrossEntropyLoss(smooth_factor=0.1)
    # CrossEntropy_fn = nn.CrossEntropyLoss()
    # focal_loss_fn = losses.FocalLoss(mode='multiclass')

    # criterion = JointLoss3(first=DiceLoss_fn, second=SoftCrossEntropy_fn, third=IoULoss_fn,
    #                         first_weight=1.0, second_weight=1.0, third_weight=1.0).cuda()
    criterion = JointLoss2(first=lovasz_fn, second=SoftCrossEntropy_fn,
                           first_weight=1.0, second_weight=1.0).cuda()

    # 主循环
    train_loss_total_epochs, valid_loss_total_epochs, epoch_lr = [], [], []
    train_loader_size = train_loader.__len__()
    valid_loader_size = valid_loader.__len__()
    best_iou = 0
    best_epoch = 0
    best_mode = copy.deepcopy(model)
    epoch_start = 0
    if load_ckpt_dir is not None:
        ckpt = torch.load(load_ckpt_dir)
        epoch_start = ckpt['epoch']
        model.load_state_dict(ckpt['state_dict'])
        optimizer.load_state_dict(ckpt['optimizer'])

    logger.info(
        'Total Epoch:{} Image_size:({}, {}) Training num:{}  Validation num:{}'.format(opt.epochs, x, y, train_data_size,
                                                                                       valid_data_size))

    for epoch in range(epoch_start, opt.epochs):
        epoch_start = time.time()
        # 训练阶段
        model.train()
        train_epoch_loss = AverageMeter()
        train_iter_loss = AverageMeter()

        logger.info(('%10s' * 4) % ('Epoch', 'loss', 'lr', "miou"))
        pbar = tqdm(enumerate(train_loader), total=len(train_loader))
        for batch_idx, batch_samples in pbar:
            optimizer.zero_grad()

            data, target = batch_samples['image'], batch_samples['label']

            data, target = Variable(data.to(device)), Variable(target.to(device))
            with autocast():
                # train_inputs, targets_a, targets_b, lam = mixup_data(data, target, 0.2, device)
                outputs = model(data)
                # loss_func = mixup_criterion(targets_a, targets_b, lam)
                loss = criterion(outputs, target)
                # pred = model(data)
                # # print(pred.shape)
                # loss = criterion(pred, target)
            scaler.scale(loss).backward()
            scaler.step(optimizer)
            scaler.update()

            scheduler.step(epoch + batch_idx / train_loader_size)

            image_loss = loss.item()
            train_epoch_loss.update(image_loss)
            train_iter_loss.update(image_loss)

            train_loss_log = train_iter_loss.avg
            train_lr_log = optimizer.param_groups[-1]['lr']

            s = ('%10s' * 1 + '%10.4g' * 3) % (
                '%g/%g' % (epoch, opt.epochs - 1), train_loss_log,train_lr_log,0)
            pbar.set_description(s)

            if batch_idx % iter_inter == 0:
                train_iter_loss.reset()

            summarywriter.add_scalar("train/lr", train_lr_log, global_step=epoch)
            summarywriter.add_scalar("train/loss", train_loss_log, global_step=epoch)

            del batch_samples
            # end train batch ------------------------------------------------------------------------------------------------

        # 验证阶段
        model.eval()
        valid_epoch_loss = AverageMeter()
        valid_iter_loss = AverageMeter()
        iou = IOUMetric(10)
        with torch.no_grad():
            for batch_idx, batch_samples in enumerate(valid_loader):
                data, target = batch_samples['image'], batch_samples['label']

                data, target = Variable(data.to(device)), Variable(target.to(device))

                with autocast():
                    # train_inputs, targets_a, targets_b, lam = mixup_data(data, target, 0.2, device)
                    # outputs = model(train_inputs)
                    # loss_func = mixup_criterion(targets_a, targets_b, lam)
                    # loss = loss_func(criterion, outputs)
                    pred = model(data)
                    loss = criterion(pred, target)

                # pred=outputs.cpu().data.numpy()
                pred = pred.cpu().data.numpy()
                pred = np.argmax(pred, axis=1)
                iou.add_batch(pred, target.cpu().data.numpy())
                #
                image_loss = loss.item()
                valid_epoch_loss.update(image_loss)
                valid_iter_loss.update(image_loss)

                # end val epoch ----------------------------------------------------------------------------------------------------

            val_loss_log = valid_iter_loss.avg
            val_acc_log, val_acc_cls_log, val_iu_log, val_mean_iu_log, val_fwavacc_log = iou.evaluate()
            print(
                "val_loss:{:.4f},val_lr:{:.4f},val_miou:{:.4f}".format(
                    val_loss_log, 0.0, val_mean_iu_log))
            summarywriter.add_scalar("val/loss", val_loss_log, global_step=epoch)
            summarywriter.add_scalar("val/miou", val_mean_iu_log, global_step=epoch)

        # 保存loss、lr
        train_loss_total_epochs.append(train_epoch_loss.avg)
        valid_loss_total_epochs.append(valid_epoch_loss.avg)
        epoch_lr.append(optimizer.param_groups[0]['lr'])
        # 保存模型
        if epoch % save_inter == 0 and epoch > min_inter:
            state = {'epoch': epoch, 'state_dict': model.state_dict(), 'optimizer': optimizer.state_dict()}
            filename = os.path.join(save_ckpt_dir, 'checkpoint-epoch{}.pth'.format(epoch))
            torch.save(state, filename)  # pytorch1.6会压缩模型，低版本无法加载
        # 保存最优模型
        if val_mean_iu_log > best_iou:  # train_loss_per_epoch valid_loss_per_epoch
            state = {'epoch': epoch, 'state_dict': model.state_dict(), 'optimizer': optimizer.state_dict()}
            filename = os.path.join(save_ckpt_dir, 'checkpoint-best.pth')
            torch.save(state, filename)
            best_iou = val_mean_iu_log
            best_mode = copy.deepcopy(model)
            logger.info('[save] Best Model saved at epoch:{} ============================='.format(epoch))

        # scheduler.step(mean_iu)
        # 显示loss
    # 训练loss曲线
    if plot:
        x = [i for i in range(opt.epochs)]
        fig = plt.figure(figsize=(12, 4))
        ax = fig.add_subplot(1, 2, 1)
        ax.plot(x, smooth(train_loss_total_epochs, 0.6), label='train loss')
        ax.plot(x, smooth(valid_loss_total_epochs, 0.6), label='val loss')
        ax.set_xlabel('Epoch', fontsize=15)
        ax.set_ylabel('CrossEntropy', fontsize=15)
        ax.set_title('train curve', fontsize=15)
        ax.grid(True)
        plt.legend(loc='upper right', fontsize=15)
        ax = fig.add_subplot(1, 2, 2)
        ax.plot(x, epoch_lr, label='Learning Rate')
        ax.set_xlabel('Epoch', fontsize=15)
        ax.set_ylabel('Learning Rate', fontsize=15)
        ax.set_title('lr curve', fontsize=15)
        ax.grid(True)
        plt.legend(loc='upper right', fontsize=15)
        plt.show()

    torch.cuda.empty_cache()  # 训练完成清空所有cuda缓存
    summarywriter.close()  # 关闭tensorboardX 日志



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model-name', type=str, default='resnet50', help='initial weights path')  # 要使用那个模型来训练
    parser.add_argument('--data', type=str, default='ours_code/net1/data/weixinyaogao.yaml', help='data.yaml path')
    parser.add_argument('--hyp', type=str, default='ours_code/net1/data/hyp.scratch.yaml', help='hyperparameters path')  # 超参数搜索
    parser.add_argument('--epochs', type=int, default=250)
    parser.add_argument('--batch-size', type=int, default=64, help='total batch size for all GPUs')
    parser.add_argument('--device', default='0', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--logdir', type=str, default='/user_data/tmp_data/', help='logging directory')
    opt = parser.parse_args()

    log_dir = increment_dir(Path(opt.logdir) / 'exp', "{}".format(opt.model_name))
    device = select_device(opt.device)

    # 准备数据集
    with open(opt.data) as f:
        data_dict = yaml.load(f, Loader=yaml.FullLoader)  # data dict

    train_data = RSCDataset(data_dict["train_imgs_dir"], data_dict["train_labels_dir"], transform=train_transform)
    valid_data = RSCDataset(data_dict["val_imgs_dir"], data_dict["val_labels_dir"], transform=val_transform)

    # 网络
    nc = int(data_dict['nc'])
    model=seg_qyl(opt.model_name,nc)
    # model= torch.nn.DataParallel(model)
    model.to(device)

    # 模型保存路径
    save_ckpt_dir = os.path.join(log_dir, 'ckpt')
    if not os.path.exists(save_ckpt_dir):
        os.makedirs(save_ckpt_dir)

    # dummy_input = torch.randn(1, 3, 256, 256).to(device)
    # onnx_path = os.path.join(log_dir,"netron_model.onnx")
    # torch.onnx.export(model, dummy_input, onnx_path)

    # 参数设置
    with open(opt.hyp, encoding='utf-8') as f:
        hyp = yaml.load(f, Loader=yaml.FullLoader)  # load hyps

    hyp['model_name'] = opt.model_name          # 模型名称
    hyp['log_dir'] = log_dir      # 日志保存路径
    hyp['save_ckpt_dir'] = save_ckpt_dir    # 权重保存路径

    # 加载权重路径（继续训练）
    hyp['load_ckpt_dir'] =  None

    # 训练
    train(hyp, opt,model, train_data, valid_data,device=device)

