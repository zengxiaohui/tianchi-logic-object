#!/usr/bin/env python

"""
Stochastic Weight Averaging (SWA)
Averaging Weights Leads to Wider Optima and Better Generalization
https://github.com/timgaripov/swa
"""
import os

import torch
from torch.cuda.amp import autocast
from tqdm import tqdm
import segmentation_models_pytorch as smp
import torch.nn as nn
from ours_code.net1.dataset import RSCDataset
from ours_code.net1.dataset import train_transform, val_transform
from ours_code.net1.models.model import seg_qyl

os.environ["CUDA_VISIBLE_DEVICES"] = "0" #"0,1"
def moving_average(net1, net2, alpha=1.):
    for param1, param2 in zip(net1.parameters(), net2.parameters()):
        param1.data *= (1.0 - alpha)
        param1.data += param2.data * alpha


def _check_bn(module, flag):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        flag[0] = True


def check_bn(model):
    flag = [False]
    model.apply(lambda module: _check_bn(module, flag))
    return flag[0]


def reset_bn(module):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        module.running_mean = torch.zeros_like(module.running_mean)
        module.running_var = torch.ones_like(module.running_var)


def _get_momenta(module, momenta):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        momenta[module] = module.momentum


def _set_momenta(module, momenta):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        module.momentum = momenta[module]


def bn_update(loader, model):
    """
        BatchNorm buffers update (if any).
        Performs 1 epochs to estimate buffers average using train dataset.
        :param loader: train dataset loader for buffers average estimation.
        :param model: model being update
        :return: None
    """
    if not check_bn(model):
        return
    model.train()
    momenta = {}
    model.apply(reset_bn)
    model.apply(lambda module: _get_momenta(module, momenta))
    n = 0

    pbar = tqdm(loader, unit="images", unit_scale=loader.batch_size)
    for batch in pbar:
        input, targets = batch['image'], batch['label']
        input = input.cuda()
        b = input.size(0)

        momentum = b / (n + b)
        for module in momenta.keys():
            module.momentum = momentum

        model(input)
        n += b

    model.apply(lambda module: _set_momenta(module, momenta))




def load_model(filepath):
    model_name = 'resnet50'#efficientnet-b4
    n_class=10
    model=seg_qyl(model_name,n_class)
    # model= torch.nn.DataParallel(model)
    checkpoints=torch.load(filepath)
    model.load_state_dict(checkpoints['state_dict'])
    model.cuda()
    return model

if __name__ == '__main__':
    import argparse
    from pathlib import Path
    from torch.utils.data import DataLoader

    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--input", type=str, default=r"/user_data/tmp_data/resnet50/ckpt/",help='input directory')
    parser.add_argument("--output", type=str, default='/user_data/tmp_data/resnet50/ckpt/swa_model.pth', help='output model file')
    parser.add_argument("--batch-size", type=int, default=16, help='batch size')
    args = parser.parse_args()

    directory = Path(args.input)
    files = [f for f in directory.iterdir() if f.suffix == ".pth"]
    assert(len(files) > 1)

    net = load_model(files[0])
    for i, f in enumerate(files[1:]):
        print(f)
        net2 = load_model(f)
        moving_average(net, net2, 1. / (i + 2))

    img_size = 128
    batch_size = 16

    data_dir = "/user_data/tmp_data/"
    train_imgs_dir = os.path.join(data_dir, "img_dir/train_val/")
    train_labels_dir = os.path.join(data_dir, "ann_dir/train_val/")
    train_data = RSCDataset(train_imgs_dir, train_labels_dir, transform=train_transform)
    train_dataloader = DataLoader(dataset=train_data, batch_size=batch_size, shuffle=True, num_workers=16,drop_last=True)
    net.cuda()
    bn_update(train_dataloader, net)

    state = {'epoch': 50, 'state_dict': net.state_dict(), 'optimizer': None}
    torch.save(state, args.output)  # pytorch1.6会压缩模型，低版本无法加载