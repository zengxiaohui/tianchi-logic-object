import cv2 as cv
from glob import glob
import numpy as np
from matplotlib import pyplot as plt
import torch
from torch import nn

def log(c, img):
    output_img = c*np.log(1.0+img)
    output_img = np.uint8(output_img+0.5)
    return output_img


def gamma(img, c, v):
    lut = np.zeros(256, dtype=np.float32)
    for i in range(256):
        lut[i] = c * i ** v
    output_img = cv.LUT(img, lut)
    output_img = np.uint8(output_img + 0.5)  # 这句一定要加上
    return output_img


def standardization(data):
    mu = np.mean(data, axis=0)
    sigma = np.std(data, axis=0)
    return (data - mu) / sigma

all_data_root = r'D:\dl\tianchi\suichang_round1_test_partA_210120'
all_file_list = glob(all_data_root+r'\*.tif')
all_data_root2 = r'D:\dl\tianchi\suichang_round1_train_210120\suichang_round1_train_210120'
all_file_list2 = glob(all_data_root2+r'\*.tif')
# result_dir = r'D:\dl\tianchi\results\results'
# for i in range(10):
#     img = cv.imread(all_file_list[i],-1)
#     # img_enhance = log(20,img)
#     img_enhance = np.zeros_like(img[:,:,:3])
#     img_enhance[:, :, 0] = img[:, :, 3]
#     img_enhance[:, :, 1] = img[:, :, 0]
#     img_enhance[:, :, 2] = img[:, :, 1]
#     name = all_file_list[i][-11:-4]
#     result = cv.imread(result_dir+name+'.png',-1)
#     plt.figure()
#     plt.subplot(121)
#     plt.imshow(img[:,:,:3])
#     plt.subplot(122)
#     plt.imshow(img_enhance)
#     plt.show()


#
# img = cv.imread(all_data_root+r'\000001.png',-1)
# # img = img[:,:,:3]
# plt.imshow(img)
# plt.show()
# img = img.reshape((256,256,1))
# img = np.transpose(img,(2,0,1))
# img_tensor = torch.from_numpy(img).float()
# img_tensor /= 255
# img_tensor = img_tensor.unsqueeze(0)
# sobel_conv = nn.Conv2d(1,1,3,bias=False,padding=1)
# # sobel_kernel = np.array([[[1,1,1],[1,-8,1],[1,1,1]],
# #                          [[1,1,1],[1,-8,1],[1,1,1]],
# #                          [[1,1,1],[1,-8,1],[1,1,1]]],dtype='float32')
# sobel_kernel = np.array([[[1,1,1],[1,-8,1],[1,1,1]]],dtype='float32')
# sobel_kernel = sobel_kernel.reshape((1,1,3,3))
# sobel_conv.weight.data = torch.from_numpy(sobel_kernel)
# edge = sobel_conv(img_tensor)
#
# edge = edge.data.squeeze().numpy()
# edge = standardization(edge)
#
# plt.imshow(edge)
# plt.show()

import numpy as np
import albumentations as A

r = []
g = []
b = []

std_r = []
std_g =[]
std_b = []


for i in range(3000):
    img = cv.imread(all_file_list[i],-1)
    img = img[:,:,:3]
    # target = cv.imread(all_file_list2[i],-1)
    # target = target[:,:,:3]
    # aug = A.Compose([A.FDA([target], p=1, read_fn=lambda x: x)])
    # img = aug(image=img)['image']

    img1 = (1.0*img)/255.0
    r.append(np.mean(img1[:,:,0]))
    g.append(np.mean(img1[:,:,1]))
    b.append(np.mean(img1[:,:,2]))

    std_r.append(np.std(img1[:,:,0]))
    std_g.append(np.std(img1[:, :, 1]))
    std_b.append(np.std(img1[:, :, 2]))


rmean = np.mean(np.array(r))
gmean = np.mean(np.array(g))
bmean = np.mean(np.array(b))

print("rmean:",rmean*255)
print("gmean:",gmean*255)
print("bmean:",bmean*255)

stdr = np.mean(np.array(std_r))
stdg = np.mean(np.array(std_g))
stdb = np.mean(np.array(std_b))

print("stdr:",stdr)
print("stdg:",stdg)
print("stdb:",stdb)

# img_ids = [f[-10:-4] for f in all_file_list]
# img_ids = sorted(img_ids)
# TEST_FILE = 'test.txt'
# with open(TEST_FILE,'w') as f:
#     for i in img_ids:
#         f.write(str(i).zfill(6)+'\n')
#
# img_ids = [f.split('\\')[-1][:-4] for f in all_file_list]
# img_ids = sorted(img_ids)
# TRAINVAL_FILE = 'trainval.txt'
# with open(TRAINVAL_FILE,'w') as f:
#     for i in img_ids:
#         f.write(str(i).zfill(6)+'\n')
#
# VAL_PER = 0.2
# N = len(img_ids)
# idx = np.arange(N)
# np.random.seed(2020)
# np.random.shuffle(idx)
# val_num = int(VAL_PER*N)
# #
# TRAIN_FILE = 'train.txt'
# VAL_FILE = 'val.txt'
#
# train_idx = idx[:-val_num]
# train_idx = sorted(train_idx)
# with open(TRAIN_FILE,'w') as f:
#     for i in train_idx:
#         f.write(str(img_ids[i]).zfill(6)+'\n')
# test_idx = idx[-val_num:]
# test_idx = sorted(test_idx)
# with open(VAL_FILE,'w') as f:
#     for i in test_idx:
#         f.write(str(img_ids[i]).zfill(6)+'\n')


