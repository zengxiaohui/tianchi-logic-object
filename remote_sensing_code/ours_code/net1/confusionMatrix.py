# 根据训练集真值和预测值，统计混淆矩阵
# 统计错分最多的图片
import glob
import numpy as np
import cv2
from matplotlib import pyplot as plt

img_weight = np.load('img_weight.npy')
print(np.where(img_weight==np.max(img_weight)))
plt.plot(img_weight)
plt.show()
#
# class_weight = np.array([0.1, 0.1, 1.0, 0.1, 5.0,
#                          0.1, 5.0, 1.0, 1.0, 1.0])
# label_files = glob.glob("D:\\dl\\tianchi\\suichang_round1_train_210120\\suichang_round1_train_210120\\*.png")
# length = len(label_files)
# img_weight = np.zeros(length)
# for i in range(length):
#     label_file = label_files[i]
#     label = cv2.imread(label_file,-1)
#     class_pixel_num = np.array([np.sum(label==i) for i in range(1,11)])
#     img_weight[i] = np.sum(class_pixel_num*class_weight)
#
# np.save('img_weight.npy',img_weight)



# for i in range(len(pred_files)):
#     pred_file = pred_files[i]
#     label_file = label_root + pred_file.split('\\')[-1]
#     confusion_matrix = np.zeros([10, 10],dtype=np.uint64)
#     pred_label = cv2.imread(pred_file,-1)
#     true_label = cv2.imread(label_file,-1)
#
#     for i,j in zip(pred_label.flatten(),true_label.flatten()):
#         if i!=j:
#             confusion_matrix[j-1,i-1] += 1
#     confusion_matrix[0,0]=0
#     confusion_matrix[1, 1] = 0
#     confusion_matrix[0, 1] = 0
#     confusion_matrix[1, 0] = 0
#     oa = np.sum(pred_label==true_label)/65536
#     print(pred_file.split('\\')[-1]+"oa:",oa)
#     print(confusion_matrix)
#     plt.imshow(confusion_matrix,cmap="gray")
#     plt.show()
#     print(1)
