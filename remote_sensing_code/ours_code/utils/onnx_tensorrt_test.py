import time

import cv2
import onnx
import onnx_tensorrt.backend as backend
import numpy as np

model = onnx.load("/user_data/model_data/checkpoint-best.onnx")
engine = backend.prepare(model, device='CUDA:0')
input_data = np.random.random(size=(1, 3, 256, 256)).astype(np.float16)
# output_data = engine.run(input_data)[0]
# print(output_data)
# print(output_data.shape)

image = cv2.imread("/user_data/tmp_data/img_dir/val/0_000001.jpg", cv2.IMREAD_COLOR)
image_cv = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image_cv = cv2.resize(image_cv, (256, 256))
mean = np.array([0.485, 0.456, 0.406])
std = np.array([0.229, 0.224, 0.225])
img_np = np.array(image_cv, dtype=float) / 255.
img_np = (img_np - mean) / std
img_np = img_np.transpose((2, 0, 1))
img_np_nchw = np.expand_dims(img_np, 0).astype(np.float16)
img_np_nchw = np.array(img_np_nchw, dtype=img_np_nchw.dtype, order='C')
print(img_np_nchw.shape,input_data.shape)
start=time.time()
output_data = engine.run(img_np_nchw)[0]

print(time.time()-start)
print(output_data.shape,type(output_data))
data = np.argmax(output_data, axis=1)
data=data.squeeze()
print(data.shape)