import os
import torch


def main():
    model_dir = r"/home/admin/tianchi/tianchi-logic-object/haining/hrnet/output/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300/"
    save_swa_path =  r'/home/admin/tianchi/tianchi-logic-object/haining/hrnet/output/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300/swa_model.pth'
    start_epoch = 300
    end_epoch = 309
    epoch_ids = list(range(start_epoch, end_epoch + 1))
    model_names = [os.path.join(model_dir, 'final_state_{}.pth'.format(i)) for i in epoch_ids]
    model_checkpoints = [torch.load(model_name) for model_name in model_names]
    num = len(model_names)
    save_model = model_checkpoints[0]
    swa_checkpoints = model_checkpoints[0].copy()
    keys = swa_checkpoints.keys()

    for key in keys:
        sum_weights = 0.0
        for model_checkpoint in model_checkpoints:
            sum_weights += model_checkpoint[key]
        average_weights = sum_weights / num
        swa_checkpoints[key] = average_weights
    save_model['model'] = swa_checkpoints

    torch.save(swa_checkpoints, save_swa_path)
    print('Model is saved at ',save_swa_path)


if __name__ == '__main__':
    main()




