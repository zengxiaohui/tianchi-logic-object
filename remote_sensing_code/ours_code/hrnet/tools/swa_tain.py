#!/usr/bin/env python

"""
Stochastic Weight Averaging (SWA)
Averaging Weights Leads to Wider Optima and Better Generalization
https://github.com/timgaripov/swa
"""
import os
import sys
sys.path.insert(0, '/home/admin/tianchi/tianchi-logic-object/haining/hrnet/lib/')
import torch
from torch.cuda.amp import autocast
from tqdm import tqdm
import segmentation_models_pytorch as smp
import torch.nn as nn
from config import config
from config import update_config
from models.seg_hrnet import get_seg_model
import argparse
from pathlib import Path
from torch.utils.data import DataLoader

from datasets.tc import TC

os.environ["CUDA_VISIBLE_DEVICES"] = "1" #"0,1"
def moving_average(net1, net2, alpha=1.):
    for param1, param2 in zip(net1.parameters(), net2.parameters()):
        param1.data *= (1.0 - alpha)
        param1.data += param2.data * alpha


def _check_bn(module, flag):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        flag[0] = True


def check_bn(model):
    flag = [False]
    model.apply(lambda module: _check_bn(module, flag))
    return flag[0]


def reset_bn(module):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        module.running_mean = torch.zeros_like(module.running_mean)
        module.running_var = torch.ones_like(module.running_var)


def _get_momenta(module, momenta):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        momenta[module] = module.momentum


def _set_momenta(module, momenta):
    if issubclass(module.__class__, torch.nn.modules.batchnorm._BatchNorm):
        module.momentum = momenta[module]


def bn_update(loader, model):
    """
        BatchNorm buffers update (if any).
        Performs 1 epochs to estimate buffers average using train dataset.
        :param loader: train dataset loader for buffers average estimation.
        :param model: model being update
        :return: None
    """
    if not check_bn(model):
        return
    model.train()
    momenta = {}
    model.apply(reset_bn)
    model.apply(lambda module: _get_momenta(module, momenta))
    n = 0

    pbar = tqdm(loader, unit="images", unit_scale=loader.batch_size)
    for batch in pbar:
        input, labels, _, _ = batch
        input = input.cuda()
        b = input.size(0)

        momentum = b / (n + b)
        for module in momenta.keys():
            module.momentum = momentum

        model(input)
        n += b

    model.apply(lambda module: _set_momenta(module, momenta))


def load_model(filepath):
    model = get_seg_model(config)
    model_dict = model.state_dict()
    pretrained_dict = torch.load(filepath)
    pretrained_dict = {
        k[6:]: v for k, v in pretrained_dict.items() if k[6:] in model_dict.keys()
    }
    model_dict.update(pretrained_dict)
    model.load_state_dict(pretrained_dict)
    return model

def parse_args():
    parser = argparse.ArgumentParser(description="Train segmentation network")

    parser.add_argument(
        "--cfg", help="experiment configure file name",
        default="/home/admin/tianchi/tianchi-logic-object/haining/hrnet/experiments/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300.yaml"
        , type=str
    )
    parser.add_argument("--local_rank", type=int, default=0)
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )
    parser.add_argument("--input", type=str, default=r"/home/admin/tianchi/tianchi-logic-object/haining/hrnet/output/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300/",help='input directory')
    parser.add_argument("--output", type=str, default='/home/admin/tianchi/tianchi-logic-object/haining/hrnet/output/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300/swa_model.pth', help='output model file')
    parser.add_argument("--batch-size", type=int, default=16, help='batch size')

    args = parser.parse_args()
    update_config(config, args)

    return args

if __name__ == '__main__':
    args = parse_args()
    directory = Path(args.input)
    start_epoch = 300
    end_epoch = 309
    epoch_ids = list(range(start_epoch, end_epoch + 1))
    files = [os.path.join(directory, 'final_state_{}.pth'.format(i)) for i in epoch_ids]
    assert(len(files) > 1)

    save_model = torch.load(files[0])

    net = load_model(files[0])
    for i, f in enumerate(files[1:]):
        print(f)
        net2 = load_model(f)
        moving_average(net, net2, 1. / (i + 2))

    img_size = 256
    batch_size = 32

    crop_size = (config.TRAIN.IMAGE_SIZE[1], config.TRAIN.IMAGE_SIZE[0])
    train_dataset = TC(
        root=config.DATASET.ROOT,
        list_path=config.DATASET.TRAIN_SET,
        num_samples=None,
        num_classes=config.DATASET.NUM_CLASSES,
        multi_scale=config.TRAIN.MULTI_SCALE,
        flip=config.TRAIN.FLIP,
        ignore_label=config.TRAIN.IGNORE_LABEL,
        base_size=config.TRAIN.BASE_SIZE,
        crop_size=crop_size,
        downsample_rate=config.TRAIN.DOWNSAMPLERATE,
        scale_factor=config.TRAIN.SCALE_FACTOR,
    )

    trainloader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=8,
        pin_memory=True,
        drop_last=False,
    )
    net.cuda()

    bn_update(trainloader, net)

    netstate_dict = net.state_dict()
    modelkeys = save_model.keys()
    for key in save_model.keys():
        save_model[key] = net.state_dict()[key[6:]]

    torch.save(save_model, args.output)  # pytorch1.6会压缩模型，低版本无法加载