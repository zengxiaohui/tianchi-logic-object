from torch import nn
from torch.nn.modules.loss import _Loss
from torch.nn import functional as F

__all__ = ["JointLoss2", "JointLoss3", "WeightedLoss"]


class WeightedLoss(_Loss):
    """Wrapper class around loss function that applies weighted with fixed factor.
    This class helps to balance multiple losses if they have different scales
    """

    def __init__(self, loss, weight=1.0):
        super().__init__()
        self.loss = loss
        self.weight = weight

    def forward(self, *input):
        score,target = (input)
        ph, pw = score.size(2), score.size(3)
        h, w = target.size(1), target.size(2)
        if ph != h or pw != w:
            score = F.interpolate(
                    input=score, size=(h, w), mode='bilinear',align_corners=True)
        return self.loss(score,target) * self.weight

class JointLoss2(_Loss):
    """
    Wrap two loss functions into one. This class computes a weighted sum of two losses.
    """

    def __init__(self, first: nn.Module, second: nn.Module, first_weight=1.0, second_weight=1.0):
        super().__init__()
        self.first = WeightedLoss(first, first_weight)
        self.second = WeightedLoss(second, second_weight)

    def forward(self, *input):
        return self.first(*input) + self.second(*input)

class JointLoss3(_Loss):
    """
    Wrap three loss functions into one. This class computes a weighted sum of three losses.
    """

    def __init__(self, first: nn.Module, second: nn.Module, third: nn.Module, first_weight=1.0, second_weight=1.0, third_weight=1.0):
        super().__init__()
        self.first = WeightedLoss(first, first_weight)
        self.second = WeightedLoss(second, second_weight)
        self.third = WeightedLoss(third, third_weight)

    def forward(self, *input):
        return self.first(*input) + self.second(*input) + self.third(*input)
