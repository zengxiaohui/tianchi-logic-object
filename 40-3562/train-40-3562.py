import os
from tqdm import tqdm
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import skimage.io as skimg_io
import cv2

from paddlex.seg import transforms

#### paddlex原生通过gdal读取tiff图像，我安装gdal时总是报错，就修改了读取图像的接口，改为skimage读取
import MyTransforms
import paddlex as pdx

#### 这里需改为解压后的数据路径。请自行新建数据集或者下载数据。
TRAIN_DIR = 'data/suichang_round1_train_210120'
TEST_DIR = 'data/suichang_round1_test_partA_210120'

print(pdx.__version__)

#### 提取训练集样本id
labeled_img_ids = list(set([f[:-4] for f in os.listdir(TRAIN_DIR) if f[-3:]=='tif']))
test_img_ids = [f[:-4] for f in os.listdir(TEST_DIR)]

print(len(labeled_img_ids), len(test_img_ids))

#### 划分训练集和验证集，并按paddlex的格式要求保存
N_CLASSES = 10
VALID_PERCENT = 0.2

N = len(labeled_img_ids)
# N = 1000
idx = np.arange(N)
np.random.seed(2021)
np.random.shuffle(idx)
val_num = int(VALID_PERCENT * N)

LABEL_FILE = 'work/dataset/labels.txt'
TRAIN_FILE = 'work/dataset/train_list.txt'
VALID_FILE = 'work/dataset/val_list.txt'

with open(LABEL_FILE, 'w') as f:
    for i in range(N_CLASSES):
        f.write(str(i)+'\n')

with open(TRAIN_FILE, 'w') as f:
    for i in idx[:-val_num]:
        img = labeled_img_ids[i]+'.tif'
        ann = labeled_img_ids[i]+'.png'
        info = img + ' ' + ann + '\n'
        f.write(info)

with open(VALID_FILE, 'w') as f:
    for i in idx[-val_num:]:
        img = labeled_img_ids[i]+'.tif'
        ann = labeled_img_ids[i]+'.png'
        info = img + ' ' + ann + '\n'
        f.write(info)

#### 设置类别权重
weights = np.array([1.0 for _ in range(N_CLASSES)])
weights[2:9] = 2.0
weights[9] = 5.0

weights = list(weights)
print(type(weights), weights)

#### 设置训练和预测的预处理pipeline
train_transforms = MyTransforms.Compose([
    transforms.RandomHorizontalFlip(),
    transforms.Normalize(mean=[0., 0., 0., 0.],
                         std=[0.5, 0.5, 0.5, 0.5],
                         min_val=[0, 0, 0, 0],
                         max_val=[255.0, 255.0, 255.0, 255.0])
])
eval_transforms = MyTransforms.Compose([
    transforms.Normalize(mean=[0., 0., 0., 0.],
                         std=[0.5, 0.5, 0.5, 0.5],
                         min_val=[0, 0, 0, 0],
                         max_val=[255.0, 255.0, 255.0, 255.0])
])
print('Make transforms.')

#### 打包训练集和验证集
train_dataset = pdx.datasets.SegDataset(
    data_dir=TRAIN_DIR,
    file_list=TRAIN_FILE,
    label_list=LABEL_FILE,
    transforms=train_transforms,
    shuffle=True)
eval_dataset = pdx.datasets.SegDataset(
    data_dir=TRAIN_DIR,
    file_list=VALID_FILE,
    label_list=LABEL_FILE,
    transforms=eval_transforms)

#### 一键生成HRNet模型，并开始训练~~~
WIDTH = 64
BATCH_SIZE = 64
LR_BASE = 0.0001
LR = LR_BASE*BATCH_SIZE
EPOCHS = 200
SAVE_DIR = 'Models/HRNet64'
SAVE_INTERVAL = 5

model = pdx.seg.HRNet(
    num_classes=N_CLASSES,
    width=WIDTH,
    class_weight=weights,
    input_channel=4,
)
print('Start train.')
model.train(
    num_epochs=EPOCHS,
    train_dataset=train_dataset,
    train_batch_size=BATCH_SIZE, #12占用约25G
    eval_dataset=eval_dataset,
    learning_rate=LR,
    log_interval_steps=200,
    save_interval_epochs=SAVE_INTERVAL,
    save_dir=SAVE_DIR,
    # resume_checkpoint='Models/HRNet64/epoch_45', #需要从断点继续训练时可以将该参数设置为模型保存的路径
    use_vdl=True)