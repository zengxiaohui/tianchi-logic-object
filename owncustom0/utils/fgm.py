import torch


class FGM():
    #借鉴的https://fyubang.com/2019/10/15/adversarial-train/
    def __init__(self, model):
        self.model = model
        self.backup = {}

    def attack(self, epsilon=1., emb_name="transformer.embeddings.word_embeddings.weight"):
        # emb_name这个参数要换成你模型中embedding的参数名
        for name, param in self.model.named_parameters():
            if param.requires_grad: #and emb_name in name
                self.backup[name] = param.data.clone()
                if param.grad is not None:
                    norm = torch.norm(param.grad)
                    if norm != 0:
                        r_at = epsilon * param.grad / norm
                        param.data.add_(r_at)

    def restore(self, emb_name="transformer.embeddings.word_embeddings.weight"):
        # emb_name这个参数要换成你模型中embedding的参数名
        for name, param in self.model.named_parameters():
            if param.requires_grad: #and emb_name in name
                assert name in self.backup
                param.data = self.backup[name]
        self.backup = {}