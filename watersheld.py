import copy

import matplotlib.pyplot as plt
from scipy import ndimage as ndi
from skimage import morphology, color, data, filters, feature
import cv2
import os
import numpy as np

from cvkmeans3 import mask_replace_num, Segment
from test_transforms import draw

filename="000003"
image_o = cv2.imread(os.path.join(r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\suichang_round1_test_partA_210120",filename + ".tif"))
image_o = cv2.cvtColor(image_o, cv2.COLOR_BGR2RGB)

image =color.rgb2gray(image_o)
denoised = filters.rank.median(image, morphology.disk(4)) #过滤噪声
#
# #将梯度值低于10的作为开始标记点
markers = filters.rank.gradient(denoised, morphology.disk(4)) <10
markers = ndi.label(markers)[0]

# image=image.astype("uint8")
# ret,binary = cv2.threshold(image,0,255,cv2.THRESH_BINARY|cv2.THRESH_OTSU)
#
# # morphology operation
# kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(1,1))#结构元素
# mb = cv2.morphologyEx(binary,cv2.MORPH_OPEN,kernel,iterations=1)#进行2次开操作
# sure_bg = cv2.dilate(mb,kernel,iterations=1)#进行3次膨胀

#distace transform
#对mb进行距离变换
# dist = cv2.distanceTransform(mb,cv2.DIST_L2,3)
# dist_output = cv2.normalize(dist,0,1.0,cv2.NORM_MINMAX)
#
# ret,surface = cv2.threshold(dist,dist.max()*0.6,255,cv2.THRESH_BINARY)
#
# surface_fg = np.uint8(surface)#转成int
# unknow = cv2.subtract(sure_bg,surface_fg)
# ret,markers = cv2.connectedComponents(surface_fg)

# markers = cv2.imread(os.path.join(r"C:\Users\zengxh\Desktop\results", filename + ".png"), cv2.IMREAD_GRAYSCALE)
# maskset = set(markers_o.flatten().tolist())
# maskkmeans = copy.deepcopy(markers_o)
# for index, classnum in enumerate(maskset):
#     maskkmeans = mask_replace_num(maskkmeans, classnum, index)
#
# seg = Segment(len(maskset), maskkmeans)
# markers, result = seg.kmeans(image_o)

# markers = cv2.imread(os.path.join(r"C:\Users\zengxh\Desktop\results", filename + ".png"), cv2.IMREAD_GRAYSCALE)
gradient = filters.rank.gradient(denoised, morphology.disk(4)) #计算梯度
labels =morphology.watershed(gradient, markers, mask=image) #基于梯度的分水岭算法


#现在我们用分水岭算法分离两个圆
# distance = ndi.distance_transform_edt(image) #距离变换
# local_maxi =feature.peak_local_max(distance, indices=False, footprint=np.ones((3, 3)),labels=image)   #寻找峰值
# markers = ndi.label(local_maxi)[0] #初始标记点
# labels =morphology.watershed(-distance, markers, mask=image) #基于距离变换的分水岭算法
# mask = cv2.imread(os.path.join(r"C:\Users\zengxh\Desktop\results", filename + ".png"), cv.IMREAD_GRAYSCALE)
# draw(image_o, markers_o)
# draw(image_o, labels)

fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(6, 6))
axes = axes.ravel()
ax0, ax1, ax2, ax3 = axes


ax0.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
ax0.set_title("Original")

cmap = plt.cm.get_cmap("Spectral")

ax1.imshow(gradient, cmap=cmap, interpolation='nearest')
ax1.set_title("Gradient")
ax2.imshow(markers, cmap=cmap, interpolation='nearest')
ax2.set_title("Markers")
ax3.imshow(labels, cmap=cmap, interpolation='nearest')
ax3.set_title("Segmented")

for ax in axes:
    ax.axis('off')

fig.tight_layout()
plt.show()