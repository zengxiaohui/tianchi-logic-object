import cv2
import numpy as np
import glob
from matplotlib import pyplot as plt
from PIL import Image
def unique(ar, return_index=False, return_inverse=False, return_counts=False):
    ar = np.asanyarray(ar).flatten()

    optional_indices = return_index or return_inverse
    optional_returns = optional_indices or return_counts

    if ar.size == 0:
        if not optional_returns:
            ret = ar
        else:
            ret = (ar,)
            if return_index:
                ret += (np.empty(0, np.bool),)
            if return_inverse:
                ret += (np.empty(0, np.bool),)
            if return_counts:
                ret += (np.empty(0, np.intp),)
        return ret
    if optional_indices:
        perm = ar.argsort(kind='mergesort' if return_index else 'quicksort')
        aux = ar[perm]
    else:
        ar.sort()
        aux = ar
    flag = np.concatenate(([True], aux[1:] != aux[:-1]))

    if not optional_returns:
        ret = aux[flag]
    else:
        ret = (aux[flag],)
        if return_index:
            ret += (perm[flag],)
        if return_inverse:
            iflag = np.cumsum(flag) - 1
            inv_idx = np.empty(ar.shape, dtype=np.intp)
            inv_idx[perm] = iflag
            ret += (inv_idx,)
        if return_counts:
            idx = np.concatenate(np.nonzero(flag) + ([ar.size],))
            ret += (np.diff(idx),)
    return ret
def colorEncode(labelmap, colors, mode='RGB'):
    labelmap = labelmap.astype('int')
    labelmap_rgb = np.zeros((labelmap.shape[0], labelmap.shape[1], 3),
                            dtype=np.uint8)
    for label in unique(labelmap):
        if label < 0:
            continue
        labelmap_rgb += (labelmap == label)[:, :, np.newaxis] * \
            np.tile(colors[label],
                    (labelmap.shape[0], labelmap.shape[1], 1))

    if mode == 'BGR':
        return labelmap_rgb[:, :, ::-1]
    else:
        return labelmap_rgb
from scipy.io import loadmat
def visualize_result(pred):
    #
    # img=cv2.imread(img_dir)
    colors = loadmat('D:\\dl\\tianchi\\unet_eff\\demo\\color150.mat')['colors']
    names = {
            1: "耕地",
            2: "林地",
            3: "草地",
            4: "道路",
            5: "城镇建设用地",
            6: "农村建设用地",
            7: "工业用地",
            8: "构筑物",
            9: "水域",
            10: "裸地"
        }
    # print predictions in descending order
    pred = np.int32(pred)
    pixs = pred.size
    uniques, counts = np.unique(pred, return_counts=True)
    #
    print("_____________")
    for idx in np.argsort(counts)[::-1]:
        name = names[uniques[idx]]
        ratio = counts[idx] / pixs * 100
        if ratio > 0.1:
            print("  {}: {:.2f}%".format(name, ratio))

    # colorize prediction
    pred_color = colorEncode(pred, colors).astype(np.uint8)

    # aggregate images and save
    #print(pred_color.shape)
    #pred_color=cv2.resize(pred_color,(256,256))
    # im_vis = np.concatenate((img, pred_color), axis=1)

    return pred_color

img_files = glob.glob(r"D:\dl\tianchi\test"+"\\*.jpg")
label_files = glob.glob(r"D:\dl\tianchi\unet_eff\results\results"+"\\*.png")
img = cv2.imread(img_files[6], -1)
label1 = cv2.imread("000007.png",-1)
label2 = cv2.imread(label_files[6],-1)
plt.subplot(221)
plt.imshow(img)
plt.subplot(222)
plt.imshow(visualize_result(label1))

plt.subplot(223)
plt.imshow(visualize_result(label2))
plt.show()
#
# for i in range(3000):
#     label = cv2.imread(label_files[i],-1)
#     label[label==3]=2
#     label[label==10]=5
#
#     img = Image.fromarray(label)
#
#     img = img.convert('L')
#     img.save(label_files[i])
    # img = cv2.imread(img_files[i], -1)
    # grass = np.sum(label==3)
    # if grass>0:
    #     plt.subplot(121)
    #     plt.imshow(img)
    #     plt.subplot(122)
    #     plt.imshow(visualize_result(label))
    #     plt.show()

