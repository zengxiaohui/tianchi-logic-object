#### 环境准备
```shell script
conda create -n py38_mmseg_tianchi_submit --offline python=3.8
conda activate py38_mmseg_tianchi_submit
cd submit
pip install -r requirements.txt
pip install torch==1.7.0 torchvision==0.8.1 torchaudio==0.7.0
pip install mmcv-full==1.2.0+torch1.7.0+cu102 -f https://download.openmmlab.com/mmcv/dist/index.html
cd code/mmsegmentation
pip install -e .  # or "python setup.py develop"

cd ../../
git clone https://github.com/alexandrosstergiou/SoftPool.git
cd SoftPool/pytorch
make install
make test

将losses文件夹copy到/home/deploy/anaconda3/envs/py38_mmseg_tianchi_submit/lib/python3.8/site-packages/segmentation_models_pytorch/
将modules.py copy到/home/deploy/anaconda3/envs/py38_mmseg_tianchi_submit/lib/python3.8/site-packages/segmentation_models_pytorch/base/
```

##### 安装mmsegmentation
[INSTALL.md](code/mmsegmentation/README.md)

##### 安装softpool
> https://github.com/alexandrosstergiou/SoftPool.git

#### train

#### test
```shell
./test.sh
```

```shell script
# 预测第一个model的结果
mv code/resnet_3693.py code/mmsegmentation/mmseg/models/backbones/resnet.py
cd code/mmsegmentation
# 修改baseline3693.py中testB的路径
python tools/test.py configs/TianchiSeg/baseline3693.py ../../user_data/model_data/iter_80000_3693.pth
mv ../../user_data/model_data/results/ ../../user_data/tmp_data/results3693
# 预测第二个模型的结果
cd ../../
mv code/resnet_3927.py code/mmsegmentation/mmseg/models/backbones/resnet.py
cd code/mmsegmentation
# 修改baseline3927.py中testB的路径
python tools/test.py configs/TianchiSeg/baseline3927.py ../../user_data/model_data/iter_120000_3927.pth
mv ../../user_data/model_data/results/ ../../user_data/tmp_data/results3927
# 预测第三个模型的结果
cd ../../
cd code/net/
# 修改tif_jpg.py，make_dataset.py，infer.py中testB的路径
python tif_jpg.py
python make_dataset.py
#将losses文件夹copy到/home/deploy/anaconda3/envs/py38_mmseg_tianchi_submit/lib/python3.8/site-packages/segmentation_models_pytorch/
#将modules.py copy到/home/deploy/anaconda3/envs/py38_mmseg_tianchi_submit/lib/python3.8/site-packages/segmentation_models_pytorch/base/
python infer.py

#集成3个模型的结果
cd ..
# 这里运行torch1.6.0+cpu版本就可以和我们提交的0.4022一样的结果了，目前这边安装的是1.7.0
# pip install torch==1.6.0
python integrate-finally-model.py
# 使用cvkmeans对结果进行后处理
python cvkmeans3.py
```