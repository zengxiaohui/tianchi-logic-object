# 2021全国绿色生态创新大赛-生态资产智能分析-非官方基线
该基线采用paddleX实现，提交分数为0.35左右，还有很大的提升空间，欢迎fork、star~

[飞桨官网](https://www.paddlepaddle.org.cn/)

[欢迎使用PaddleX！](https://paddlex.readthedocs.io/zh_CN/develop/index.html)

大赛地址：[2021全国数字生态创新大赛-智能算法赛](https://tianchi.aliyun.com/competition/entrance/531860/introduction?spm=5176)

### 赛题背景
本赛题基于不同地形地貌的高分辨率遥感影像资料，希望参赛者能够利用遥感影像智能解译技术识别提取土地覆盖和利用类型，实现生态资产盘点、土地利用动态监测、水环境监测与评估、耕地数量与监测等应用。
结合现有的地物分类实际需求，参照地理国情监测、“三调”等既有地物分类标准，设计陆域土地覆盖与利用类目体系，包括：林地、草地、耕地、水域、道路、城镇建设用地、农村建设用地，工业用地、构筑物、裸地。

### 赛题数据说明
（1）数据简介： 数据为覆盖0.8m-2m分辨率的高分系列遥感多光谱影像，成像波段包括R、G、B、Nir波段，数据覆盖地貌包括：山地、丘陵地区、河湖（水库）、平原、城镇等等。浙江大学环境学院为赛题提供了数据支持。<br>
（2）数据规格：4万+张遥感影像及对应地物分类标记样本，影像大小为256 * 256像素。<br>
初赛：16017张高分遥感影像和标注文件训练集，A榜测试集3000张测试数据，B榜测试集4366张测试数据。<br>
复赛：15904张高分遥感影像和标注文件，6000张测试数据。<br>
（3）训练测试数据说明：<br>
影像保存格式为tif文件，包括R、G、B、Nir四个波段，训练测试集影像尺寸均为256 * 256像素。标签数据格式为单通道的png。<br>
（4）评价指标为mIoU。

**其它详细信息请参见比赛官网。**

Baseline基于paddleX实现，仅修改了数据读取模块以避免gdal安装报错的问题。

notebook为训练部分，预测代码见predict.py，可以不中断训练在后台运行，随时生成提交结果。predict.py同时对结果进行了可视化，并将预测结果和可视化结果分别压缩保存，下载后即可在比赛官网上传。

MyTransforms.py为修改的paddleX中的Compose函数，一是修改了图像读取部分为skimage读取，二是修改了标签图像的读取。官方标签范围为[1,10]，读取时减去1以变为0起始。相应的在预测时对结果加1。

运行时需要在work目录下手动生成output、dataset等目录（懒得写代码生成了）

数据请在官网报名比赛后自行下载，谢谢~

### 飞浆
> https://paddlex.readthedocs.io/zh_CN/develop/appendix/parameters.html

### 杭州
```shell script
conda deactivate
conda create -n tianchi-logic-object --offline python=3.8
conda activate tianchi-logic-object

```

### 经验
1. 比赛不管每次训练后的测试结果如何差都不能放弃
2. 使用论坛中开放的代码，在这些基础上修改
3. 问群里比较厉害的大佬的方案
4. 每次训练的结果要保存起来
5. 最后15天要开始把整体框架方案，和代码，权重整理好（基本就在此基础上修改了）
6. 一定要有时间打比赛才行

### 参考资料

参考资料

https://tianchi.aliyun.com/forum/postDetail?spm=5176.12586969.1002.6.25ea3df1P3F7EZ&postId=78945

https://tianchi.aliyun.com/forum/postDetail?spm=5176.12282027.0.0.5a10379cQNGy2O&postId=79094

https://zhuanlan.zhihu.com/p/330994781

https://github.com/zhanghang1989/ResNeSt

https://github.com/NVIDIA/semantic-segmentation

https://github.com/tensorflow/tpu/tree/master/models/official/detection/projects/self_training

https://www.kaggle.com/artgor/segmentation-in-pytorch-using-convenient-tools#

https://github.com/Megvii-BaseDetection/DynamicRouting

https://zhuanlan.zhihu.com/p/351749327

https://pytorch.org/docs/stable/optim.html

### 提交代码
```shell script
ossutil64.exe cp C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\submit.zip oss://tianchi-race-upload/result/race-code/531860/718/431566/1095279343427/1615512430414_submit.zip -i STS.NStYGYc2MXEPR21LWoT5BaHzb -k Gto8TmGCsuvKMsL2HoMsgUi9XQpfrcqhf2JZG2XQdvz9 --endpoint=oss-cn-hangzhou.aliyuncs.com --sts-token=CAIS6gN1q6Ft5B2yfSjIr5DBEv3tju1s74e7UBSAqFc6WLpujo3RgDz2IHxEe3ZhAu0bsv4zmmpW7/8Slr5vQKgYHhWUM5NHt8wGrFn+MtGd4JDvsuTyTHxv8y/BZSTg1er+Ps85JrG0I4WaCT3tkit03sG7F1GLVECkNpukkINuas9tMCCzcTtBAqUvRGxls9RIEXrKKdywOR3nnnGqTygOgAdnjn5l4qmS29CV7gGk7Gf30egIvY/8UP/GCsBnJ8V4SMznnr40JOizeUcyhjFO8KBp9vwmnR7MotCaBXRI/hyCKJC629ZzLQh/FOBYfqlft6rEiOFfsO7enJiVrBFWJrNxUj/DY4qizcDYA5m/VLADeK38Jm7G3/2WK5DxqHlEW38AMx5QcNdTIB14ABc3GDbBMf3lqhKYfgaqTaOF0eQs2JF4ikjw+d2DKx+VTq6E1TpfIJY3YgYjMBIbmjG6K/9YK0sTKgMgW+3MENstK0kG+P6y4lyJCnY8lC4Q5qOnOq6P4PxEMdmmAqUE8/JEOM0f4zp1Fw2vGuzw1xdJTgE/H+YKipuKEISk9bqIzN+UZeP7Ef0dsj1YC2uN/3XSGCgNcHGruoN5NwPBq96s0K3F9ZhgEU424tlDAwafdJN1tVtq83p6NuI1G0aJGoABfbjmdXyCOWhx5Qx3Ga35pcsF5HYOKxTdzTiheTvkO9P0o0faMYTtLMhQe2bNtfnPVTfNVP7DLy9LJHAZw0B/poQWDUiAbSVCk5ETf4vTF04NqJRlsDYONBhdSna3Uf7gOKZZQqLCkJHCT9pc4ZKQtJ7W80O9VXyoSmSEHu+GJco=```